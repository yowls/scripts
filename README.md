<img src=".img/banner.png" align=center height=300px>

## Welcome!
In this place you will find all the scripts that i use on my linux machine <br>
All the scripts have been baptized with `chmod +x`

**Recommend:** make symbolic link or copy files to some folder on your path

<br>
<br>

## Tree of files

    ├─ ..
    │
    ├─ crontab/
    │  └─ montly-rsync		Montly backup with rsync
    │
    ├─ notify/
    │  ├─ screenshot            Take screenshots using maim
    │  ├─ volume                Change volume and Microphone
    │  └─ brigthness            Change brightness of screen using xbacklight
    │
    ├─ update/
    │  ├─ update-kitty		update kitty terminal
    │  ├─ update-qtile		update qtile window manager
    │  └─ update-Youtube-dl	update youtube-dl
    │
    └─ utilities/
       ├─ compress		Compress in a given archive
       ├─ extract		extrat from compress archive
       └─ ssh-askpass		auto add a ssh key
<br>
<br>

## Important
Under no circumstances I am responsible for any kind of damage derive from the use of this on your machine.<br>
If you lose files, if your install breaks something, if something explodes.<br>
My scripts comes with no warranties.<br>
Again; you shouldn't play with my toys if you don't want to get hurt. 
(This doesn't mean my scripts will hurt you, you know, but this is legal boilerplate to cover my ass in case something happens).
